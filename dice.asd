;;;; dice.asd

(asdf:defsystem #:dice
  :description "Library for managing dice pools"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.1"
  :depends-on (#:metabang-bind
               #:mgl-pax)
  :components
  ((:file "package")
   (:module "source"
    :depends-on ("package")
    :components
    ((:file "dice")
     (:file "pool"
      :depends-on ("dice"))
     (:file "manipulation"
      :depends-on ("dice"
                   "pool"))
     ))))
