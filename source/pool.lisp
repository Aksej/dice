(in-package #:dice)

(defsection @pool (:title "Dice Pools")
  (<dice-pool> function)
  (pool<- (reader [dice-pool]))
  (ordering<- (reader [dice-pool]))
  (reason<- (reader [dice-pool]))
  (parents<- (reader [dice-pool]))
  (with-dice-pool macro)
  (@manipulations section))

(defclass [dice-pool] ()
  ((%pool :type list
          :initarg :pool
          :reader pool<-)))

(defun <dice-pool> (pool)
  (make-instance '[dice-pool]
                 :pool pool))
