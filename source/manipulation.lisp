(in-package #:dice)

(defsection @manipulations (:title "Dice pool manipulations")
  (throw-dice function)
  (pool-union function)
  (reorder-pool function)
  (discard-highest-dice function)
  (discard-lowest-dice function)
  (keep-highest-dice function)
  (keep-lowest-dice function)
  (duplicate-highest-dice function)
  (duplicate-lowest-dice function)
  (duplicate-highest-dice-if function)
  (duplicate-lowest-dice-if function)
  (replace-highest-dice-if function)
  (replace-lowest-dice-if function)
  (filter-dice function)
  (modify-dice function)
  (modify-pool function)
  (top-die function)
  (bottom-die function)
  (sum-dice function)
  (count-dice function)
  (pool-average function))

(defun throw-dice (dice)
  (mapcan #'roll-die
          (ensure-dice dice)))

(defun pool-union (&rest pools)
  (<dice-pool> (mapcan #'pool<-
                       pools)))

(defun sort-pool (pool ordering)
  (<dice-pool> (stable-sort (pool<- pool)
                            ordering)))

(defun discard-dice (pool
                     &key
                       count
                       (predicate (constantly t)))
  (bind ((pool (pool<- pool)))
    (<dice-pool> (if count
                     (loop
                       :for n :from 0
                       :for roll :in pool
                       :unless (and (< n count)
                                    (funcall predicate roll))
                         :collect roll)
                     (loop
                       :for roll :in pool
                       :unless (funcall predicate roll)
                         :collect roll)))))

(defun keep-dice (pool
                  &key
                    count
                    (predicate (constantly t)))
  (bind ((pool (pool<- pool)))
    (<dice-pool> (if count
                     (loop
                       :for n :from 0
                       :for roll :in pool
                       :when (and (< n count)
                                  (funcall predicate roll))
                         :collect roll)
                     (loop
                       :for roll :in pool
                       :when (funcall predicate roll)
                         :collect roll)))))

(defun duplicate-dice (pool
                       &key
                         count
                         (predicate (constantly t)))
  (bind ((pool (pool<- pool)))
    (<dice-pool> (append (if count
                             (loop
                               :repeat count
                               :for roll :in pool
                               :when (funcall predicate roll)
                                 :append (roll-die (die<- roll)))
                             (loop
                               :for roll :in pool
                               :when (funcall predicate roll)
                                 :append (roll-die (die<- roll))))
                         pool))))

(defun replace-dice (pool
                     &key
                       count
                       (predicate (constantly t)))
  (bind ((pool (pool<- pool)))
    (<dice-pool> (if count
                     (loop
                       :for n :from 0
                       :for roll :in pool
                       :append (if (and (< n count)
                                        (funcall predicate roll))
                                   (roll-die (die<- roll))
                                   (list roll)))
                     (loop
                       :for roll :in pool
                       :append (if (funcall predicate roll)
                                   (roll-die (die<- roll))
                                   (list roll)))))))

(defun modify-dice (pool fn)
  (<dice-pool> (mapcar fn (pool<- pool))))

(defun modify-pool (pool fn)
  (<dice-pool> (funcall fn (pool<- pool))))

(defun top-die (dice-pool)
  (first (pool<- dice-pool)))

(defun bottom-die (dice-pool)
  (car (last (pool<- dice-pool))))

(defun sum-dice (dice-pool)
  (reduce #'+ (pool<- dice-pool)))

(defun count-dice (dice-pool)
  (length (pool<- dice-pool)))

(defun pool-average (dice-pool)
  (loop
    :for n :from 0
    :for roll :in (print (pool<- dice-pool))
    :for sum := (result<- roll)
      :then (+ sum (result<- roll))
    :finally
       (return (if (zerop n)
                   0
                   (/ sum n)))))
