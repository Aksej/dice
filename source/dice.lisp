(in-package #:dice)

(defsection @die (:title "Dice")
  (roll-die function)
  (die<-symbol function)
  (symbol<-die function)
  (ensure-die function)
  (ensure-dice function)
  ([die] class)
  (<die> function)
  (roll-die (method () ([die])))
  (symbol<-die (method () ([die])))
  ([exploding-die] class)
  (<exploding-die> function)
  (roll-die (method () ([exploding-die])))
  (symbol<-die (method () ([exploding-die])))
  ([expanding-die] class)
  (<expanding-die> function)
  (roll-die (method () ([expanding-die])))
  (symbol<-die (method () ([expanding-die]))))

(defclass [die] ()
  ((%sides :type (integer 2)
           :initarg :sides
           :reader sides<-)))

(defmethod print-object ((object [die])
                         stream)
  (format stream "#<~A~A>"
          (symbol<-die object)
          (sides<- object)))

(defun <die> (sides)
  "Returns a die with SIDES sides. Rolls a value between 1 and SIDES
(inclusive)."
  (make-instance '[die]
                 :sides sides))

(defclass [exploding-die] ([die])
  ((%smooth? :type boolean
             :initarg :smooth?
             :reader smooth?)))

(defun <exploding-die> (sides
                        &optional
                          (smooth? t))
  (make-instance '[exploding-die]
                 :sides sides
                 :smooth? smooth?))

(defclass [expanding-die] ([die])
  ())

(defun <expanding-die> (sides)
  (make-instance '[expanding-die]
                 :sides sides))

(defclass [roll] ()
  ((%die :type [die]
         :initarg :die
         :reader die<-)
   (%result :type (integer 1)
            :initarg :result
            :reader result<-)))

(defmethod print-object ((object [roll])
                         stream)
  (format stream "#<ROLL ~A → ~A>"
          (die<- object)
          (result<- object)))

(defgeneric roll-die (die)
  (:method ((die [die]))
    (list (make-instance '[roll]
                         :die die
                         :result (1+ (random (sides<- die))))))
  (:method ((die [exploding-die]))
    (list (make-instance '[roll]
                         :die die
                         :result (bind ((sides (sides<- die))
                                        (rep (1- sides))
                                        (smooth? (smooth? die)))
                                   (+ (loop
                                        :for roll := (random sides)
                                        :sum (if smooth?
                                                 roll
                                                 (1+ roll))
                                        :while (= roll rep))
                                      (if smooth?
                                          1
                                          0))))))
  (:method ((die [expanding-die]))
    (bind ((sides (sides<- die)))
      (loop
        :for roll := (1+ (random sides))
        :collect (make-instance '[roll]
                                :die die
                                :result roll)
        :while (= roll sides)))))

(defgeneric die<-symbol (symbol sides)
  (:method ((symbol (eql :d))
            (sides integer))
    (<die> sides))
  (:method ((symbol (eql :x))
            (sides integer))
    (<exploding-die> sides nil))
  (:method ((symbol (eql :e))
            (sides integer))
    (<exploding-die> sides))
  (:method ((symbol (eql :p))
            (sides integer))
    (<expanding-die> sides)))

(defgeneric symbol<-die (die)
  (:method ((die [die]))
    :d)
  (:method ((die [exploding-die]))
    (if (smooth? die)
        :e
        :x))
  (:method ((die [expanding-die]))
    :p))


(defun ensure-die (die)
  (etypecase die
    ([die]
     die)
    (list
     (bind (((symbol sides)
             die))
       (check-type symbol symbol)
       (check-type sides integer)
       (die<-symbol symbol sides)))))


(defun ensure-dice (dice)
  (loop
    :for die :in dice
    :append
    (etypecase dice
      ([die]
       (list die))
      (list
       (bind (((symbol/integer &rest args)
               die))
         (etypecase symbol/integer
           (symbol
            (bind (((sides)
                    args))
              (check-type sides integer)
              (list (die<-symbol symbol/integer sides))))
           (integer
            (loop
              :repeat symbol/integer
              :collect (ensure-die args)))))))))
