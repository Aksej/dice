;;;; package.lisp

(mgl-pax:define-package #:dice
  (:use #:cl #:mgl-pax)
  (:shadowing-import-from
   #:metabang-bind
   #:bind)
  #+nil
  (:export #:[die] #:<die> #:[exploding-die] #:<exploding-die>
           #:[expanding-die] #:<expanding-die> #:sides<-
           #:roll-die #:die<-symbol #:symbol<-die #:ensure-die #:ensure-dice
           #:<dice-pool> #:pool<- #:ordering<- #:reason<- #:parents<-
           #:with-dice-pool
           #:defdpm #:defdpms
           #:throw-dice #:empty-pool #:pool-union #:reorder-pool
           #:discard-highest-dice #:discard-lowest-dice #:keep-highest-dice
           #:keep-lowest-dice #:duplicate-highest-dice #:duplicate-lowest-dice
           #:duplicate-highest-dice-if #:duplicate-lowest-dice-if
           #:replace-highest-dice-if #:replace-lowest-dice-if #:modify-dice
           #:filter-dice #:modify-pool
           #:top-die #:bottom-die #:sum-dice #:count-dice #:pool-average))


(in-package #:dice)

(defsection @dice (:title "Dice library for managing dice pools")
  (dice asdf/system:system)
  (@interface section))

(defsection @interface (:title "Interface")
  (@die section)
  (@pool section))
